<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('form');
    }

    public function welcome(){
        return "Selamat Datang, Tamu";
    }

    public function welcome_post(Request $request){
        $data = $request->all();
        return view('welcome', ["data" => $data]);
    }
    
}
